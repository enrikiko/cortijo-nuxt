package steps;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class Urls {

  private WebDriver driver;
  private String homeUrl = "http://test.cortijodemazas.com/";

  @Before
  public void setup() {
    WebDriverManager.chromedriver().setup();
    this.driver = new ChromeDriver();
    this.driver.manage().window().maximize();
    this.driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
    this.driver.manage().timeouts().setScriptTimeout(60, TimeUnit.SECONDS);
  }

  @After
  public void teardown() throws InterruptedException {
    Thread.sleep(3000);
    this.driver.manage().deleteAllCookies();
    this.driver.quit();
    this.driver = null;
  }

  @Given("User at home page")
  public void user_at_home_page() {
    driver.get(homeUrl);
  }

  @When("User clicks on Negocios")
  public void user_clicks_on_negocios() throws InterruptedException {
    Thread.sleep(2000);
    driver.findElement(By.linkText("Negocios")).click();
  }

  @Then("He is redirected to Negocios page")
  public void he_is_redirected_to_negocios_page() {
    String negociosUrl = homeUrl + "negocios/";
    Assert.assertEquals(negociosUrl, driver.getCurrentUrl());
  }
}
